## Moving around on terminal
#### Navigate to beginning
```
ctrl + a
```

#### Navigate to end
```
ctrl + e
```

#### Back by one word
```
alt + b
```

#### Forward by one word
```
alt + f
```

#### Delete by one word back
```
alt + <--
```

#### Delete by one word forward
```
alt + D
```
#
## Bash commands
#### Show location current directory
```bash
pwd
```

#### Show date
```bash
date
```

#### Show calendar
```bash
cal
```

#### Show full calendar
```bash
cal <year>
```

#### Show logged user
```bash
whoami
```

#### Show command manual
```bash
man <command>
```

#### Create intermediary folders
```bash
mkdir -p /example/example-1/hello/qwerty.py
```

#### Show tree of directories
```bash
tree
```

#### Write or override some code to the file
```bash
echo "select * from customer;" > query.sql
```

#### Append the file
```bash
echo "select * from order;" >> query.sql
```

#### Paginate the file. Defaul per 10 lines
```bash
less <filename>
```

#### See fist 10 lines
```bash
head <filename>
```

#### See last 10 lines
```bash
tail <filename>
```

#### 4 ways to show content of file
```bash
cat | less | head | tail
```

#### Copy
```bash
cp <from> <to>
```

#### Move or rename
```bash
mv <from> <to>
```

#### Find everything from current directory
```bash
find .
```

#### Find by name
```bash
find . -name <filename>
```

#### Find by type and name
```bash
find . -type f -name <filename>
```

#### Find by type and name with insensitive case
```bash
find . -type f -iname <filename>
```

#### Find empty folders
```bash
find . -empty
```

#### Delete file or folders with find condition
```bash
find . -type f -name <filename> -delete
```

#### Grep. Command for search from files. Find lines of "END" word from examples directory
```bash
grep -rn "END" examples
```

#### Find lines of given string with insensitive mode
```bash
grep -rni "customer" .
```

#### Find lines and show 1 line after and 1 line before
```bash
grep -rni -A 1 -B 1 "customer" .
```

#### Grep from result of another command
```bash
cat <filename> | grep "some string"
```

#### Change last command word
```bash
^<oldname>^<newname> + Enter
```

#### Get last command (Don't use as a root user)
```bash
!! + Enter
```

#### Search from backward and forward history
```bash
ctrl + r
```
```
ctrl + s
```

#### Download files with curl
```bash
curl -O <url_path>
```

