<!-- Headings -->
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

<!-- Italics -->
*This text* is italic

\*This text* is not italic

_This text_ is italic

<!-- Strong -->
**This text** is strong

__This text__ is strong

<!-- Strikethrough -->
~~This text~~ is strikethrough

<!-- Horizontal Rule -->
---
___

<!-- Blockquote -->
>This is a quote

<!-- Links -->
[Nuriddin Obidjonov](http://example.com "Nuriddin Obidjonov")

<!-- UL -->
* Item 1
* Item 2
* item 3
    * Nested item 1
    * Nested item 2
    * Nested item 3

<!-- OL -->
1. Item 1
2. Item 2
3. Item 3

<!-- Inline code block -->
`<p> This is a paragraph </p>`

<!-- Images -->
![MarkDown Logo](https://markdown-here.com/img/icon256.png)

<!-- Github Markdown -->

```bash
    npm install
    npm start
```

```javascript
    function add(num1, num2) {
        return num1 + num2;
    }
```

```python
    print("Hello world")
```

<!-- Tables -->
| Name     | Email                           |
| -------- | ------------------------------- | 
| Nuriddin | Nuriddin.Obidjonov777@gmail.com |

<!-- Task Lists -->
* [x] Task 1
* [x] Task 2
* [ ] Task 3
